// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ShipController.h"
#include "Components/CapsuleComponent.h"
#include "Engine.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Reef.generated.h"

UCLASS()
class GAMEPROJECT_API AReef : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AReef();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	AShipController* shipController;

	UCapsuleComponent* capsuleCollision;
	bool isHit = false;

	//5909642059 Naruebet Tirasirichai
	void Get_CapsuleCollision();

	//5909642141 Thanachit Loca-apichai
	UFUNCTION()
		void OnShipBeginOverlap(class UPrimitiveComponent* HitComp, class AActor* OtherActor,
			class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
