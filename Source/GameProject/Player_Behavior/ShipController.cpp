// Fill out your copyright notice in the Description page of Project Settings.


#include "ShipController.h"
#include "Components/StaticMeshComponent.h"
#include "Engine.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AShipController::AShipController()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AShipController::BeginPlay()
{
	Super::BeginPlay();

	Get_BodyRef();
	SetIsHide(false);
	Get_BoxCollision();
	boxCollision->OnComponentBeginOverlap.AddDynamic(this, &AShipController::OnPierBeginOverlap);
	//playerUI = Cast<UPlayerInterface>(GetOwner());
}

// Called every frame
void AShipController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Body_Behavior();
}

// Called to bind functionality to input
void AShipController::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("MoveBackAndForth", this, &AShipController::MoveBackAndForth);
}

void AShipController::MoveBackAndForth(float Val) {
	FRotator currentRotation = bodyRef->GetComponentRotation();
	FVector forwardDir = FRotationMatrix(currentRotation).GetScaledAxis(EAxis::X);
	AddMovementInput(-forwardDir, Val);
}

void AShipController::Get_BodyRef(){
	TArray<UStaticMeshComponent*> components;
	this->GetComponents<UStaticMeshComponent>(components);

	for (int32 index = 0; index < components.Num(); index++) {
		if (components[index]->GetFName().ToString() == "BodyReference") {
			bodyRef = components[index];

			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("BodyReference is set."));
		}
	}
}

void AShipController::Body_Behavior(){
	FVector ShipPosition = this->GetActorLocation();

	APlayerController* OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);
	OurPlayerController->bShowMouseCursor = true;

	FHitResult Hit;
	OurPlayerController->GetHitResultUnderCursor(ECC_Visibility, false, Hit);

	if (Hit.bBlockingHit == true) {
		FVector mouseWorldPosition = Hit.ImpactPoint;

		float xDiff = mouseWorldPosition.X - ShipPosition.X;
		float yDiff = mouseWorldPosition.Y - ShipPosition.Y;

		float turnAtAngle = atan2f(yDiff, xDiff) * 180 / 3.1415926f;

		FRotator currentRotation = bodyRef->GetComponentRotation();
		FRotator newRotation = FRotator(currentRotation.Pitch, turnAtAngle, currentRotation.Roll);

		bodyRef->SetWorldRotation(newRotation);
	}
}

void AShipController::SetIsHide(bool hide) {
	if (hide == true) {
		isHide = hide;
		// Hides visible components
		SetActorHiddenInGame(true);

		// Disables collision components
		SetActorEnableCollision(false);

		// Stops the Actor from ticking
		SetActorTickEnabled(false);

		//playerUI->fuel_Panel->ClearChildren();
	}

	if(hide == false) {
		isHide = hide;
		// Hides visible components
		SetActorHiddenInGame(false);

		// Disables collision components
		SetActorEnableCollision(true);

		// Stops the Actor from ticking
		SetActorTickEnabled(true);
	}
}

bool AShipController::GetIsHide() {
	return isHide;
}

void AShipController::Get_BoxCollision()
{
	TArray<UBoxComponent*> components;
	GetComponents<UBoxComponent>(components);

	for (int32 index = 0; index < components.Num(); index++) {
		if (components[index]->GetFName().ToString() == "BoxCollision") {
			boxCollision = components[index];
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("Get_BoxCollision is set."));
		}
	}
}

void AShipController::OnPierBeginOverlap(UPrimitiveComponent * HitComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherComp->ComponentHasTag("Pier")) {
		SetIsHide(true);
		UPanelWidget* fuelPanel = GetOwner()->FindComponentByClass<UPlayerInterface>()->fuel_Panel;
		//fuelPanel->Visibility = ESlateVisibility::Hidden;
	}
}