// Fill out your copyright notice in the Description page of Project Settings.


#include "Reef.h"

// Sets default values
AReef::AReef()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AReef::BeginPlay()
{
	Super::BeginPlay();

	Get_CapsuleCollision();
	capsuleCollision->OnComponentBeginOverlap.AddDynamic(this, &AReef::OnShipBeginOverlap);
	
}

// Called every frame
void AReef::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AReef::Get_CapsuleCollision()
{
	TArray<UCapsuleComponent*> components;
	this->GetComponents<UCapsuleComponent>(components);

	for (int32 index = 0; index < components.Num(); index++) {
		if (components[index]->GetFName().ToString() == "CapsuleCollision") {
			capsuleCollision = components[index];
		}
	}
}

void AReef::OnShipBeginOverlap(UPrimitiveComponent * HitComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (OtherComp->ComponentHasTag("Ship")) {
		shipController = Cast<AShipController>(OtherActor);
		shipController->SetIsHide(true);
		UPanelWidget* fuelPanelWidget = (shipController->FindComponentByClass<UPlayerInterface>()->fuel_Panel);
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, fuelPanelWidget->GetName());
		fuelPanelWidget->SetVisibility(ESlateVisibility::Hidden);
		//fuelPanelWidget->RemoveFromParent();
	}
}

