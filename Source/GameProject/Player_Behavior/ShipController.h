// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../PlayerInterface.h"
#include "Components/BoxComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ShipController.generated.h"

UCLASS()
class GAMEPROJECT_API AShipController : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AShipController();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//5909642141 Thanachit Loca-apichai
	UFUNCTION()
		void MoveBackAndForth(float Val);

	//UPlayerInterface* playerUI;

	UStaticMeshComponent* bodyRef;
	bool isHide;
	
	UBoxComponent* boxCollision;
	//bool isHit = false;

	//5909642059 Naruebet Tirasirichai
	void Get_BodyRef();
	//5909642059 Naruebet Tirasirichai
	void Body_Behavior();
	//6009644060 Tanet Netsuwan
	void SetIsHide(bool hide);
	bool GetIsHide();

	//5909642059 Naruebet Tirasirichai
	void Get_BoxCollision();

	//5909642141 Thanachit Loca-apichai
	UFUNCTION()
		void OnPierBeginOverlap(class UPrimitiveComponent* HitComp, class AActor* OtherActor,
			class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};




