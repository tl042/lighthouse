// Fill out your copyright notice in the Description page of Project Settings.


#include "TopdownMode.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"

ATopdownMode::ATopdownMode() {
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnObject(TEXT("Pawn'/Game/Blueprints/BP_ShipController1.BP_ShipController1_C'"));

	if (PlayerPawnObject.Class != NULL) {
		DefaultPawnClass = PlayerPawnObject.Class;
	}
}