// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TopdownMode.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API ATopdownMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	//5909642059 Naruebet Tirasirichai
	ATopdownMode();
	
};
