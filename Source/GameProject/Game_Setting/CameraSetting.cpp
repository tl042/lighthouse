// Fill out your copyright notice in the Description page of Project Settings.


#include "CameraSetting.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ACameraSetting::ACameraSetting()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACameraSetting::BeginPlay()
{
	Super::BeginPlay();

	APlayerController* OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);
	OurPlayerController->SetViewTarget(TopdownCamera);
}

// Called every frame
void ACameraSetting::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

