// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerInterface.h"

// Sets default values for this component's properties
UPlayerInterface::UPlayerInterface()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	mainUIWidget = StaticLoadClass(UObject::StaticClass(), nullptr, TEXT("/Game/Blueprints/HUD/HUD_Main.HUD_Main_C"));
}


// Called when the game starts
void UPlayerInterface::BeginPlay()
{
	Super::BeginPlay();

	GetOwner()->InputComponent->BindAction("PauseMenu", IE_Pressed, this, &UPlayerInterface::Toggle_PauseMenu).bExecuteWhenPaused = true;
	//shipController = Cast<AShipController>(GetOwner());

	// ...
	if (mainUIWidget != NULL) {
		mainUI = CreateWidget<UMainWidget>(GetWorld(), mainUIWidget);
		if (mainUI != NULL) {
			mainUI->AddToViewport();

			UWidgetTree* widgetTree = mainUI->WidgetTree;
			
			FString widgetString = "Timer_Text";
			FName widgetName = FName(*widgetString);

			UWidget* targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL) {
				textTimer = Cast<UTextBlock>(targetWidget);
			}

			//Progress Timer Bar
			widgetString = "Timer_Progress";
			widgetName = FName(*widgetString);

			targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL) {
				timerProgressBar = Cast<UProgressBar>(targetWidget);
			}

			//Progress Fuel Bar &  Panel
			widgetString = "PauseMenu_Panel";
			widgetName = FName(*widgetString);

			targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL) {
				pause_Panel = Cast<UPanelWidget>(targetWidget);
			}

			widgetString = "fuel_Panel";
			widgetName = FName(*widgetString);

			targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL) {
				fuel_Panel = Cast<UPanelWidget>(targetWidget);
			}

			widgetString = "Fuel_Progress";
			widgetName = FName(*widgetString);

			targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL) {
				fuelProgressBar = Cast<UProgressBar>(targetWidget);
			}

			//PauseMenu
			widgetString = "PauseMenu_Panel";
			widgetName = FName(*widgetString);

			targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL) {
				pause_Panel = Cast<UPanelWidget>(targetWidget);
			}

			//Resume button
			widgetString = "Resume_Button";
			widgetName = FName(*widgetString);

			targetWidget = widgetTree->FindWidget(widgetName);

			if (targetWidget != NULL) {
				resume_Button = Cast<UButton>(targetWidget);
				resume_Button->OnClicked.AddDynamic(this, &UPlayerInterface::Toggle_PauseMenu);
			}
		}
	}
	
}


// Called every frame
void UPlayerInterface::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// ...
	//timer
	levelTimer -= GetWorld()->GetDeltaSeconds();

	if (levelTimer < 0) {
		levelTimer = 0;
	}

	FText levelTimerText = UKismetTextLibrary::Conv_FloatToText(levelTimer, ERoundingMode::FromZero, false, false, 1, 2, 1, 1);
	textTimer->SetText(levelTimerText);

	timerProgressBar->SetPercent(levelTimer / 60.f);

	FVector2D ScreenLocation;
	APlayerController* playerController = GetWorld()->GetFirstPlayerController();
	APlayerController* OurPlayerController = UGameplayStatics::GetPlayerController(playerController, 0);
	UGameplayStatics::ProjectWorldToScreen(OurPlayerController, playerController->GetPawn()->GetActorLocation(), ScreenLocation, false);
	Set_FuelBarToShip(ScreenLocation);
	
	//Fuel
	levelFuel -= GetWorld()->GetDeltaSeconds();

	if (levelFuel < 0) {
		levelFuel = 0;
		//shipController = Cast<AShipController>(shipController);
		//shipController->SetIsHide(true);
	}

	fuelProgressBar->SetPercent(levelFuel / 25.f);
}

void UPlayerInterface::Toggle_PauseMenu() {
	if (onPause == false) {
		onPause = true;

		UCanvasPanelSlot* pausePanelSlot = Cast<UCanvasPanelSlot>(pause_Panel->Slot);
		pausePanelSlot->SetPosition(FVector2D(0, 0));

		UGameplayStatics::SetGamePaused(GetWorld(), true);

		//enable show cusor when pause menu
		APlayerController* playerController = GetWorld()->GetFirstPlayerController();
		playerController->SetInputMode(FInputModeGameAndUI());
	}
	else{
		onPause = false;

		UCanvasPanelSlot* pausePanelSlot = Cast<UCanvasPanelSlot>(pause_Panel->Slot);
		pausePanelSlot->SetPosition(FVector2D(-2000, 0));
		
		UGameplayStatics::SetGamePaused(GetWorld(), false);

		//disable show cusor when close pause menu
		APlayerController* playerController = GetWorld()->GetFirstPlayerController();
		playerController->SetInputMode(FInputModeGameOnly());
	}
}

void UPlayerInterface::Set_FuelBarToShip(FVector2D position) {
	FVector2D ViewportSize;
	FVector2D ViewportCenter;
	FVector2D CanvasSize;
	FVector2D Divisor;
	float DPIScaling;

	GetWorld()->GetGameViewport()->GetViewportSize(ViewportSize);
	ViewportCenter = ViewportSize / 2.0f;

	// Root Panel Data
	CanvasSize = mainUI->GetDesiredSize();
	Divisor = CanvasSize / ViewportSize;
	DPIScaling = GetDefault<UUserInterfaceSettings>()->GetDPIScaleBasedOnSize(FIntPoint(ViewportSize.X, ViewportSize.Y));

	UCanvasPanelSlot* canvasPanelSlot = Cast<UCanvasPanelSlot>(fuel_Panel->Slot);
	const FVector2D AnchorDivided = ((position - ((canvasPanelSlot->GetSize() * DPIScaling) / 2.f)) * Divisor) / CanvasSize;
	const FAnchors NewAnchors = FAnchors(AnchorDivided.X, AnchorDivided.Y, AnchorDivided.X, AnchorDivided.Y);

	canvasPanelSlot->SetAnchors(NewAnchors);
	canvasPanelSlot->SetPosition(FVector2D::ZeroVector);
}

