// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MainWidget.h"
#include "Blueprint/UserWidget.h"
#include "Runtime/UMG/Public/Blueprint/WidgetTree.h"
#include "Runtime/UMG/Public/Components/Image.h"
#include "Runtime/UMG/Public/Components/Button.h"
#include "Runtime/UMG/Public/Components/TextBlock.h"
#include "Runtime/UMG/Public/Components/ProgressBar.h"
#include "Runtime/UMG/Public/Components/EditableTextBox.h"
#include "Runtime/UMG/Public/Components/CanvasPanelSlot.h"
#include "Kismet/KismetTextLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/UserInterfaceSettings.h"

#include "Player_Behavior/ShipController.h"
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlayerInterface.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMEPROJECT_API UPlayerInterface : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerInterface();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	TSubclassOf<class UUserWidget> mainUIWidget;

	UMainWidget* mainUI;

	UTextBlock* textTimer;
	float levelTimer = 60.0f;

	UProgressBar* timerProgressBar;

	UPanelWidget* pause_Panel;
	bool onPause = false;

	UButton* resume_Button;

	//AShipController* shipController;

	UPanelWidget* fuel_Panel;
	UProgressBar* fuelProgressBar;
	float levelFuel = 25.0f;
	
	void Set_FuelBarToShip(FVector2D position);

	UFUNCTION()
		void Toggle_PauseMenu();
};
